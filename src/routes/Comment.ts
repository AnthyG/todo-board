export default class Comment {
	_id: string;
	_rev: string | undefined;
	content: string;

	constructor({
		_id = undefined,
		_rev = undefined,
		content,
	}: {
		_id?: string;
		_rev?: string;
		content: string;
	}) {
		this._id = _id ?? crypto.randomUUID();
		this._rev = _rev;
		this.content = content;
	}

	update(listUpdate: Partial<Omit<ConstructorParameters<typeof Comment>[0], '_id'>>) {
		this._rev = listUpdate._rev ?? this._rev;
		this.content = listUpdate.content ?? this.content;
	}

	asJson() {
		return {
			_id: this._id,
			_rev: this._rev,
			content: this.content,
		};
	}
}
