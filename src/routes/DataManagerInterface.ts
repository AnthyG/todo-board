import type { Readable } from 'svelte/store';
import type Card from './Card';
import type List from './List';
import type Comment from './Comment';

export default interface DataManagerInterface {
	open(): Promise<void>;
	close(): Promise<void>;
	createList(name: string): Promise<void>;
	updateList(id: string, listUpdate: Parameters<List['update']>[0]): Promise<void>;
	deleteList(id: string): Promise<void>;
	getListsStore(): Readable<List[]>;
	createCard(name: string): Promise<Card>;
	updateCard(id: string, cardUpdate: Parameters<Card['update']>[0]): Promise<void>;
	deleteCard(id: string): Promise<void>;
	addCardToList(cardId: string, listId: string): Promise<void>;
	removeCardFromList(cardId: string, listId: string): Promise<void>;
	createCardInList(name: string, listId: string): Promise<void>;
	changeSortOrderOfCardsInList(cardIds: string[], listId: string): Promise<void>;
	createComment(content: string, cardId: string): Promise<void>;
	updateComment(id: string, commentUpdate: Parameters<Comment['update']>[0]): Promise<void>;
	deleteComment(id: string): Promise<void>;
	getCommentsOnCardStore(cardId: string): Readable<Comment[]>;
}
