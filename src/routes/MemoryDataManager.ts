import type { Readable } from 'svelte/store';
import { writable, derived } from 'svelte/store';
import type DataManagerInterface from './DataManagerInterface';
import List from './List';
import Card from './Card';
import Comment from './Comment';

export default class MemoryDataManager implements DataManagerInterface {
	#lists = writable<List[]>([]);

	#cards = writable<Card[]>([]);

	#comments = writable<Comment[]>([]);

	async open() {
		this.#lists.set([
			new List({
				_id: 'todo',
				name: 'todo',
				cardIds: ['a', 'b', 'c'],
			}),
			new List({
				_id: 'in-progress',
				name: 'in progress',
				cardIds: ['d', 'e', 'f'],
			}),
			new List({
				_id: 'done',
				name: 'done',
			}),
		]);
		this.#cards.set([
			new Card({
				_id: 'a',
				name: 'a',
			}),
			new Card({
				_id: 'b',
				name: 'b',
			}),
			new Card({
				_id: 'c',
				name: 'c',
			}),
			new Card({
				_id: 'd',
				name: 'd',
			}),
			new Card({
				_id: 'e',
				name: 'e',
			}),
			new Card({
				_id: 'f',
				name: 'f',
			}),
		]);
	}

	async close() {
		this.#lists.set([]);
		this.#cards.set([]);
	}

	getListsStore(): Readable<List[]> {
		return derived([this.#lists, this.#cards, this.#comments], ([$lists, $cards, $comments]) =>
			$lists.map(
				(list) =>
					new List({
						...list,
						cards: list.cardIds
							.map((cardId) => $cards.find(({ _id }) => _id === cardId))
							.filter((card): card is Card => typeof card !== 'undefined')
							.map(
								(card) =>
									new Card({
										...card,
										comments: card.commentIds
											.map((commentId) => $comments.find(({ _id }) => _id === commentId))
											.filter((comment): comment is Comment => typeof comment !== 'undefined'),
									})
							),
					})
			)
		);
	}

	async createList(name: string) {
		this.#lists.update(($lists) => [
			...$lists,
			new List({
				_id: crypto.randomUUID(),
				name,
			}),
		]);
	}

	async updateList(id: string, listUpdate: Parameters<List['update']>[0]) {
		this.#lists.update(($lists) => {
			const listIndex = $lists.findIndex(({ _id }) => _id === id);
			$lists[listIndex].update(listUpdate);
			return $lists;
		});
	}

	async deleteList(id: string) {
		this.#lists.update(($lists) => {
			const listIndex = $lists.findIndex(({ _id }) => _id === id);
			$lists.splice(listIndex, 1);
			return $lists;
		});
	}

	async createCard(name: string) {
		const card = new Card({
			name,
		});
		this.#cards.update(($cards) => [...$cards, card]);
		return card;
	}

	async addCardToList(cardId: string, listId: string) {
		this.#lists.update(($lists) => {
			const listIndex = $lists.findIndex(({ _id }) => _id === listId);
			$lists[listIndex].cardIds = [...$lists[listIndex].cardIds, cardId];
			return $lists;
		});
	}

	async removeCardFromList(cardId: string, listId: string) {
		this.#lists.update(($lists) => {
			const listIndex = $lists.findIndex(({ _id }) => _id === listId);
			const cardIndex = $lists[listIndex].cardIds.findIndex((id) => id === cardId);
			$lists[listIndex].cardIds.splice(cardIndex, 1);
			return $lists;
		});
	}

	async createCardInList(name: string, listId: string) {
		const card = await this.createCard(name);
		await this.addCardToList(card._id, listId);
	}

	async changeSortOrderOfCardsInList(cardIds: string[], listId: string) {
		this.#lists.update(($lists) => {
			const listIndex = $lists.findIndex(({ _id }) => _id === listId);
			$lists[listIndex].cardIds = cardIds;
			return $lists;
		});
	}

	async updateCard(id: string, cardUpdate: Parameters<Card['update']>[0]) {
		this.#cards.update(($cards) => {
			const cardIndex = $cards.findIndex(({ _id }) => _id === id);
			$cards[cardIndex].update(cardUpdate);
			return $cards;
		});
	}

	async deleteCard(id: string) {
		this.#cards.update(($cards) => {
			const cardIndex = $cards.findIndex(({ _id }) => _id === id);
			this.#lists.update(($lists) => {
				$lists
					.filter(({ cardIds }) => cardIds.includes(id))
					.forEach((list) => {
						const cardIdIndex = list.cardIds.findIndex((cardId) => cardId === id);
						list.cardIds.splice(cardIdIndex, 1);
					});
				return $lists;
			});
			$cards.splice(cardIndex, 1);
			return $cards;
		});
	}

	async createComment(content: string, cardId: string) {
		const comment = new Comment({
			content,
		});

		this.#comments.update(($comments) => [...$comments, comment]);

		this.#cards.update(($cards) => {
			const cardIndex = $cards.findIndex(({ _id }) => _id === cardId);
			$cards[cardIndex].commentIds = [comment._id, ...$cards[cardIndex].commentIds];
			return $cards;
		});
	}

	async updateComment(id: string, commentUpdate: Parameters<Comment['update']>[0]) {
		this.#comments.update(($comments) => {
			const commentIndex = $comments.findIndex(({ _id }) => _id === id);
			$comments[commentIndex].update(commentUpdate);
			return $comments;
		});
	}

	async deleteComment(id: string) {
		this.#cards.update(($cards) => {
			$cards
				.filter(({ commentIds }) => commentIds.includes(id))
				.forEach((card) => {
					const commentIdIndex = card.commentIds.findIndex((commentId) => commentId === id);
					card.commentIds.splice(commentIdIndex, 1);
				});
			return $cards;
		});

		this.#comments.update(($comments) => {
			const commentIndex = $comments.findIndex(({ _id }) => _id === id);
			$comments.splice(commentIndex, 1);
			return $comments;
		});
	}

	getCommentsOnCardStore(cardId: string): Readable<Comment[]> {
		return derived([this.#cards, this.#comments], ([$cards, $comments]) => {
			const card = $cards.find(({ _id }) => _id === cardId);

			if (typeof card === 'undefined') {
				return [];
			}

			return card.commentIds
				.map((commentId) => $comments.find(({ _id }) => _id === commentId))
				.filter((comment): comment is Comment => typeof comment !== 'undefined');
		});
	}
}
