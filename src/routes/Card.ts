import type Comment from './Comment';

export default class Card {
	_id: string;
	_rev: string | undefined;
	name: string;
	commentIds: string[];
	comments: Comment[];

	constructor({
		_id = undefined,
		_rev = undefined,
		name,
		commentIds = [],
		comments = [],
	}: {
		_id?: string;
		_rev?: string;
		name: string;
		commentIds?: string[];
		comments?: Comment[];
	}) {
		this._id = _id ?? crypto.randomUUID();
		this._rev = _rev;
		this.name = name;
		this.commentIds = commentIds;
		this.comments = comments;
	}

	update(cardUpdate: Partial<Omit<ConstructorParameters<typeof Card>[0], '_id'>>) {
		this._rev = cardUpdate._rev ?? this._rev;
		this.name = cardUpdate.name ?? this.name;
		this.commentIds = cardUpdate.commentIds ?? this.commentIds;
		this.comments = cardUpdate.comments ?? this.comments;
	}

	asJson() {
		return {
			_id: this._id,
			_rev: this._rev,
			name: this.name,
			commentIds: this.commentIds,
		};
	}
}
