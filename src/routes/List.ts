import type Card from './Card';

export default class List {
	_id: string;
	_rev: string | undefined;
	name: string;
	cardIds: string[];
	cards: Card[];

	constructor({
		_id = undefined,
		_rev = undefined,
		name,
		cardIds = [],
		cards = [],
	}: {
		_id?: string;
		_rev?: string;
		name: string;
		cardIds?: string[];
		cards?: Card[];
	}) {
		this._id = _id ?? crypto.randomUUID();
		this._rev = _rev;
		this.name = name;
		this.cardIds = cardIds;
		this.cards = cards;
	}

	update(listUpdate: Partial<Omit<ConstructorParameters<typeof List>[0], '_id'>>) {
		this._rev = listUpdate._rev ?? this._rev;
		this.name = listUpdate.name ?? this.name;
		this.cardIds = listUpdate.cardIds ?? this.cardIds;
		this.cards = listUpdate.cards ?? this.cards;
	}

	asJson() {
		return {
			_id: this._id,
			_rev: this._rev,
			name: this.name,
			cardIds: this.cardIds,
		};
	}
}
