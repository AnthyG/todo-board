import type { Readable } from 'svelte/store';
import { get, writable, derived } from 'svelte/store';
//import PouchDB from 'pouchdb/dist/pouchdb.js';
//import PouchDBFind from 'pouchdb/dist/pouchdb.find.js';
import type DataManagerInterface from './DataManagerInterface';
import Card from './Card';
import List from './List';
import Comment from './Comment';

async function PouchDBInitializer() {
	window['PouchDB'] = (await import('pouchdb/dist/pouchdb')).default;

	const PouchDBFind = await import('pouchdb/dist/pouchdb.find.js');
	window['PouchDB'].plugin(PouchDBFind);

	return window['PouchDB'];
}

export default class PouchDBDataManager implements DataManagerInterface {
	#lists = writable<List[]>([]);

	#cards = writable<Card[]>([]);

	#comments = writable<Comment[]>([]);

	#db;

	async open() {
		const PouchDB = await PouchDBInitializer();
		this.#db = new PouchDB('http://localhost:5984/todo-board');

		await this.#db.info();

		await this.fetchAllData();

		//		const designDocument = {
		//			_id: '_design/views',
		//			views: {
		//				lists: {
		//					map: function (doc, emit) { emit(doc.name); }.toString()
		//				},
		//				cards: {
		//					map:
		//				},
		//			},
		//		};
		//		pouch.put(designDocument)

		this.#db
			.changes({
				live: true,
				since: 'now',
				include_docs: true,
			})
			.on('change', (change) => {
				console.debug(change);
				const doc = change.doc;
				if (doc._deleted) {
					return;
				}
				if (this.#docIsList(doc)) {
					this.#lists.update(($lists) => {
						const listIndex = $lists.findIndex(({ _id }) => _id === doc._id);
						const list = new List(doc);
						if (listIndex === -1) {
							$lists = [...$lists, list];
						} else {
							$lists[listIndex] = list;
						}
						return $lists;
					});
				}
				if (this.#docIsCard(doc)) {
					this.#cards.update(($cards) => {
						const cardIndex = $cards.findIndex(({ _id }) => _id === doc._id);
						const card = new Card(doc);
						if (cardIndex === -1) {
							$cards = [...$cards, card];
						} else {
							$cards[cardIndex] = card;
						}
						return $cards;
					});
				}
				if (this.#docIsComment(doc)) {
					this.#comments.update(($comments) => {
						const commentIndex = $comments.findIndex(({ _id }) => _id === doc._id);
						const comment = new Comment(doc);
						if (commentIndex === -1) {
							$comments = [comment, ...$comments];
						} else {
							$comments[commentIndex] = comment;
						}
						return $comments;
					});
				}
			})
			.on('error', function (error) {
				console.error(error);
			});
	}

	async close() {
		return;
	}

	#docIsList(doc) {
		return 'cardIds' in doc;
	}

	#docIsCard(doc) {
		return !('cardIds' in doc);
	}

	#docIsComment(doc) {
		return 'content' in doc;
	}

	async fetchAllData() {
		const data = await this.#db.allDocs({
			include_docs: true,
		});
		this.#lists.set(
			data.rows.filter(({ doc }) => this.#docIsList(doc)).map(({ doc }) => new List(doc))
		);
		this.#cards.set(
			data.rows.filter(({ doc }) => this.#docIsCard(doc)).map(({ doc }) => new Card(doc))
		);
		this.#comments.set(
			data.rows.filter(({ doc }) => this.#docIsComment(doc)).map(({ doc }) => new Comment(doc))
		);
	}

	getListsStore(): Readable<List[]> {
		return derived([this.#lists, this.#cards, this.#comments], ([$lists, $cards, $comments]) =>
			$lists.map(
				(list) =>
					new List({
						...list,
						cards: list.cardIds
							.map((cardId) => $cards.find(({ _id }) => _id === cardId))
							.filter((card): card is Card => typeof card !== 'undefined')
							.map(
								(card) =>
									new Card({
										...card,
										comments: card.commentIds
											.map((commentId) => $comments.find(({ _id }) => _id === commentId))
											.filter((comment): comment is Comment => typeof comment !== 'undefined'),
									})
							),
					})
			)
		);
	}

	async createList(name: string) {
		const list = new List({
			name,
		});

		const responseJson = await this.#db.put({
			...list.asJson(),
			//            table: 'list',
		});
		console.debug(responseJson);
		list._rev = responseJson.rev;

		//        this.#lists.update(($lists) => [...$lists, list]);
		await this.fetchAllData();
	}

	async updateList(id: string, listUpdate: Parameters<List['update']>[0]) {
		const $lists = get(this.#lists);
		const listIndex = $lists.findIndex(({ _id }) => _id === id);
		$lists[listIndex].update(listUpdate);

		// TODO: instead of just putting, we should follow the recommended sequence: https://pouchdb.com/guides/conflicts.html
		const responseJson = await this.#db.put({
			...$lists[listIndex].asJson(),
		});
		console.debug(responseJson);
		$lists[listIndex]._rev = responseJson.rev;

		// TODO: probably not safe, as changes could have been made to other lists... maybe just fetch from DB again
		//        this.#lists.set($lists);
		await this.fetchAllData();
	}

	async deleteList(id: string) {
		const $lists = get(this.#lists);
		const listIndex = $lists.findIndex(({ _id }) => _id === id);

		const responseJson = await this.#db.put({
			...$lists[listIndex].asJson(),
			_deleted: true,
		});
		console.debug(responseJson);
		$lists[listIndex]._rev = responseJson.rev;

		await this.fetchAllData();
	}

	async createCard(name: string) {
		const card = new Card({
			name,
		});

		const responseJson = await this.#db.put({
			...card.asJson(),
			//            table: 'card',
		});
		console.debug(responseJson);
		card._rev = responseJson.rev;

		//		this.#cards.update(($cards) => [...$cards, card]);
		await this.fetchAllData();
		return card;
	}

	async addCardToList(cardId: string, listId: string) {
		const $lists = get(this.#lists);
		const listIndex = $lists.findIndex(({ _id }) => _id === listId);
		$lists[listIndex].cardIds = [...$lists[listIndex].cardIds, cardId];

		// TODO: instead of just putting, we should follow the recommended sequence: https://pouchdb.com/guides/conflicts.html
		const responseJson = await this.#db.put({
			...$lists[listIndex].asJson(),
		});
		console.debug(responseJson);
		$lists[listIndex]._rev = responseJson.rev;

		// TODO: probably not safe, as changes could have been made to other lists... maybe just fetch from DB again
		//        this.#lists.set($lists);
		await this.fetchAllData();
	}

	async removeCardFromList(cardId: string, listId: string) {
		const $lists = get(this.#lists);
		const listIndex = $lists.findIndex(({ _id }) => _id === listId);
		const cardIndex = $lists[listIndex].cardIds.findIndex((id) => id === cardId);
		$lists[listIndex].cardIds.splice(cardIndex, 1);

		// TODO: instead of just putting, we should follow the recommended sequence: https://pouchdb.com/guides/conflicts.html
		const responseJson = await this.#db.put({
			...$lists[listIndex].asJson(),
		});
		console.debug(responseJson);
		$lists[listIndex]._rev = responseJson.rev;

		// TODO: probably not safe, as changes could have been made to other lists... maybe just fetch from DB again
		//        this.#lists.set($lists);
		await this.fetchAllData();
	}

	async createCardInList(name: string, listId: string) {
		const card = await this.createCard(name);
		await this.addCardToList(card._id, listId);
	}

	async changeSortOrderOfCardsInList(cardIds: string[], listId: string) {
		const $lists = get(this.#lists);
		const listIndex = $lists.findIndex(({ _id }) => _id === listId);
		$lists[listIndex].cardIds = [...new Set(cardIds)];

		// TODO: instead of just putting, we should follow the recommended sequence: https://pouchdb.com/guides/conflicts.html
		const responseJson = await this.#db.put({
			...$lists[listIndex].asJson(),
		});
		console.debug(responseJson);
		$lists[listIndex]._rev = responseJson.rev;

		// TODO: probably not safe, as changes could have been made to other lists... maybe just fetch from DB again
		//        this.#lists.set($lists);
		await this.fetchAllData();
	}

	async updateCard(id: string, cardUpdate: Parameters<Card['update']>[0]) {
		const $cards = get(this.#cards);
		const cardIndex = $cards.findIndex(({ _id }) => _id === id);
		$cards[cardIndex].update(cardUpdate);

		// TODO: instead of just putting, we should follow the recommended sequence: https://pouchdb.com/guides/conflicts.html
		const responseJson = await this.#db.put({
			...$cards[cardIndex].asJson(),
		});
		console.debug(responseJson);
		$cards[cardIndex]._rev = responseJson.rev;

		// TODO: probably not safe, as changes could have been made to other cards... maybe just fetch from DB again
		//        this.#cards.set($cards);
		await this.fetchAllData();
	}

	async deleteCard(id: string) {
		const $lists = get(this.#lists);
		const $cards = get(this.#cards);
		const cardIndex = $cards.findIndex(({ _id }) => _id === id);

		const responseJson = await this.#db.bulkDocs([
			...$lists
				.filter(({ cardIds }) => cardIds.includes(id))
				.map((list) => {
					const cardIdIndex = list.cardIds.findIndex((cardId) => cardId === id);
					list.cardIds.splice(cardIdIndex, 1);
					return list.asJson();
				}),
			{
				...$cards[cardIndex].asJson(),
				_deleted: true,
			},
		]);
		console.debug(responseJson);
		//        $cards[cardIndex]._rev = responseJson.rev;

		await this.fetchAllData();
	}

	async createComment(content: string, cardId: string) {
		const comment = new Comment({
			content,
		});

		const $cards = get(this.#cards);

		const cardIndex = $cards.findIndex(({ _id }) => _id === cardId);
		$cards[cardIndex].commentIds = [comment._id, ...$cards[cardIndex].commentIds];

		const responseJson = await this.#db.bulkDocs([
			{
				...comment.asJson(),
			},
			{
				...$cards[cardIndex].asJson(),
			},
		]);
		console.debug(responseJson);
		comment._rev = responseJson[0].rev;
		$cards[cardIndex]._rev = responseJson[0].rev;

		await this.fetchAllData();
	}

	async updateComment(id: string, commentUpdate: Parameters<Comment['update']>[0]) {
		const $comments = get(this.#comments);
		const commentIndex = $comments.findIndex(({ _id }) => _id === id);
		$comments[commentIndex].update(commentUpdate);

		// TODO: instead of just putting, we should follow the recommended sequence: https://pouchdb.com/guides/conflicts.html
		const responseJson = await this.#db.put({
			...$comments[commentIndex].asJson(),
		});
		console.debug(responseJson);
		$comments[commentIndex]._rev = responseJson.rev;

		// TODO: probably not safe, as changes could have been made to other comments... maybe just fetch from DB again
		//        this.#comments.set($comments);
		await this.fetchAllData();
	}

	async deleteComment(id: string) {
		const $cards = get(this.#cards);
		const $comments = get(this.#comments);
		const commentIndex = $comments.findIndex(({ _id }) => _id === id);

		const responseJson = await this.#db.bulkDocs([
			...$cards
				.filter(({ commentIds }) => commentIds.includes(id))
				.map((card) => {
					const commentIdIndex = card.commentIds.findIndex((commentId) => commentId === id);
					card.commentIds.splice(commentIdIndex, 1);
					return card.asJson();
				}),
			{
				...$comments[commentIndex].asJson(),
				_deleted: true,
			},
		]);
		console.debug(responseJson);
		//        $comments[commentIndex]._rev = responseJson.rev;

		await this.fetchAllData();
	}

	getCommentsOnCardStore(cardId: string): Readable<Comment[]> {
		return derived([this.#cards, this.#comments], ([$cards, $comments]) => {
			const card = $cards.find(({ _id }) => _id === cardId);

			if (typeof card === 'undefined') {
				return [];
			}

			return card.commentIds
				.map((commentId) => $comments.find(({ _id }) => _id === commentId))
				.filter((comment): comment is Comment => typeof comment !== 'undefined');
		});
	}
}
